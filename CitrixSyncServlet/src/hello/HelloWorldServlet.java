package hello;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.peri.sap.po.ejb.citrix.sync.CitrixSync;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class HelloWorldServlet
 */
@WebServlet("/")
public class HelloWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorldServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().println("Starte Synchronisation der SST 750");

		LOGGER.info("Start der Schnittstelle");
		
		String iseHost = "172.16.10.171";
		int isePort = 9060;
		String iseUser = "API_Guest_Access";
		String isePwd = "346xtwTaHuMjvhyJ$XbQY!K3Cuzr3N";
		String mdmHost = "https://as1108.awmdm.com/api/mdm/devices/search";
		String mdmUser = "corp.peri\\srvmdm2ise";
		String mdmPwd = "n_qx.ASzJHcMLa3$5ip96kmf!RBuDt";
		String iseGroupName= "GuestEndpoints_MDM";

		
		try{
			CitrixSync citrixSync = new CitrixSync(iseHost, isePort, iseUser, isePwd, iseGroupName, mdmHost, mdmUser, mdmPwd, LOGGER);
			//citrixSync.processSync();
		}
		catch (Exception e){
		response.getWriter().println(e.getMessage());
		}
		
		response.getWriter().println("Beende Synchronisation der SST 750");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
