//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Aenderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.03.27 um 03:48:15 PM CET 
//


package com.air_watch.servicemodel.resources;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse fuer anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Page" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="PageSize" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="Total" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *         &lt;element name="Devices" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{}Id"/>
 *                   &lt;element ref="{}Uuid"/>
 *                   &lt;element name="Udid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MacAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Imei" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
 *                   &lt;element name="AssetNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DeviceFriendlyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LocationGroupId">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedShort">
 *                           &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="LocationGroupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UserId">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedInt">
 *                           &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UserEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Ownership" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PlatformId">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
 *                           &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Platform" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ModelId">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
 *                           &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Model" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OperatingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LastSeen" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="EnrollmentStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ComplianceStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CompromisedStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="LastEnrolledOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="LastComplianceCheckOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="LastCompromisedCheckOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="IsSupervised" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="DeviceMCC">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="SIMMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrentMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="VirtualMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="DeviceCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AvailableDeviceCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LastSystemSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="IsDeviceDNDEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="IsDeviceLocatorEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="IsCloudBackupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="IsActivationLockEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="IsNetworkTethered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="BatteryLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="IsRoaming" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="LastNetworkLANSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="LastBluetoothSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="SystemIntegrityProtectionEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="ProcessorArchitecture" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="UserApprovedEnrollment" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="EnrolledViaDEP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="TotalPhysicalMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="AvailablePhysicalMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="SecurityPatchDate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="SystemUpdateReceivedTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="IsSecurityPatchUpdate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="EasIds">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="EasId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "page",
    "pageSize",
    "total",
    "devices"
})
@XmlRootElement(name = "DeviceSearchResult")
public class DeviceSearchResult {

    @XmlElement(name = "Page")
    @XmlSchemaType(name = "unsignedByte")
    protected short page;
    @XmlElement(name = "PageSize")
    @XmlSchemaType(name = "unsignedByte")
    protected short pageSize;
    @XmlElement(name = "Total")
    @XmlSchemaType(name = "unsignedShort")
    protected int total;
    @XmlElement(name = "Devices", required = true)
    protected List<DeviceSearchResult.Devices> devices;

    /**
     * Ruft den Wert der page-Eigenschaft ab.
     * 
     */
    public short getPage() {
        return page;
    }

    /**
     * Legt den Wert der page-Eigenschaft fest.
     * 
     */
    public void setPage(short value) {
        this.page = value;
    }

    /**
     * Ruft den Wert der pageSize-Eigenschaft ab.
     * 
     */
    public short getPageSize() {
        return pageSize;
    }

    /**
     * Legt den Wert der pageSize-Eigenschaft fest.
     * 
     */
    public void setPageSize(short value) {
        this.pageSize = value;
    }

    /**
     * Ruft den Wert der total-Eigenschaft ab.
     * 
     */
    public int getTotal() {
        return total;
    }

    /**
     * Legt den Wert der total-Eigenschaft fest.
     * 
     */
    public void setTotal(int value) {
        this.total = value;
    }

    /**
     * Gets the value of the devices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the devices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDevices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeviceSearchResult.Devices }
     * 
     * 
     */
    public List<DeviceSearchResult.Devices> getDevices() {
        if (devices == null) {
            devices = new ArrayList<DeviceSearchResult.Devices>();
        }
        return this.devices;
    }


    /**
     * <p>Java-Klasse fuer anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{}Id"/>
     *         &lt;element ref="{}Uuid"/>
     *         &lt;element name="Udid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MacAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Imei" type="{http://www.w3.org/2001/XMLSchema}unsignedLong"/>
     *         &lt;element name="AssetNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DeviceFriendlyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LocationGroupId">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedShort">
     *                 &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="LocationGroupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UserId">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedInt">
     *                 &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UserEmailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Ownership" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PlatformId">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
     *                 &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Platform" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ModelId">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
     *                 &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Model" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OperatingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LastSeen" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="EnrollmentStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ComplianceStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CompromisedStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="LastEnrolledOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="LastComplianceCheckOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="LastCompromisedCheckOn" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="IsSupervised" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="DeviceMCC">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="SIMMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrentMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="VirtualMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="DeviceCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AvailableDeviceCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LastSystemSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="IsDeviceDNDEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="IsDeviceLocatorEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="IsCloudBackupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="IsActivationLockEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="IsNetworkTethered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="BatteryLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="IsRoaming" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="LastNetworkLANSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="LastBluetoothSampleTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="SystemIntegrityProtectionEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="ProcessorArchitecture" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="UserApprovedEnrollment" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="EnrolledViaDEP" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="TotalPhysicalMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="AvailablePhysicalMemory" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="SecurityPatchDate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="SystemUpdateReceivedTime" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="IsSecurityPatchUpdate" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="EasIds">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="EasId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "uuid",
        "udid",
        "serialNumber",
        "macAddress",
        "imei",
        "assetNumber",
        "deviceFriendlyName",
        "locationGroupId",
        "locationGroupName",
        "userId",
        "userName",
        "userEmailAddress",
        "ownership",
        "platformId",
        "platform",
        "modelId",
        "model",
        "operatingSystem",
        "phoneNumber",
        "lastSeen",
        "enrollmentStatus",
        "complianceStatus",
        "compromisedStatus",
        "lastEnrolledOn",
        "lastComplianceCheckOn",
        "lastCompromisedCheckOn",
        "isSupervised",
        "deviceMCC",
        "virtualMemory",
        "deviceCapacity",
        "availableDeviceCapacity",
        "lastSystemSampleTime",
        "isDeviceDNDEnabled",
        "isDeviceLocatorEnabled",
        "isCloudBackupEnabled",
        "isActivationLockEnabled",
        "isNetworkTethered",
        "batteryLevel",
        "isRoaming",
        "lastNetworkLANSampleTime",
        "lastBluetoothSampleTime",
        "systemIntegrityProtectionEnabled",
        "processorArchitecture",
        "userApprovedEnrollment",
        "enrolledViaDEP",
        "totalPhysicalMemory",
        "availablePhysicalMemory",
        "securityPatchDate",
        "systemUpdateReceivedTime",
        "isSecurityPatchUpdate",
        "easIds",
        "processed"
    })
    public static class Devices {

        @XmlElement(name = "Id", namespace = "")
        @XmlSchemaType(name = "unsignedShort")
        protected int id;
        @XmlElement(name = "Uuid", namespace = "", required = true)
        protected String uuid;
        @XmlElement(name = "Udid", required = true)
        protected String udid;
        @XmlElement(name = "SerialNumber", required = true)
        protected String serialNumber;
        @XmlElement(name = "MacAddress", required = true)
        protected String macAddress;
        @XmlElement(name = "Imei", required = true)
        @XmlSchemaType(name = "unsignedLong")
        protected BigInteger imei;
        @XmlElement(name = "AssetNumber", required = true)
        protected String assetNumber;
        @XmlElement(name = "DeviceFriendlyName", required = true)
        protected String deviceFriendlyName;
        @XmlElement(name = "LocationGroupId", required = true)
        protected DeviceSearchResult.Devices.LocationGroupId locationGroupId;
        @XmlElement(name = "LocationGroupName", required = true)
        protected String locationGroupName;
        @XmlElement(name = "UserId", required = true)
        protected DeviceSearchResult.Devices.UserId userId;
        @XmlElement(name = "UserName", required = true)
        protected String userName;
        @XmlElement(name = "UserEmailAddress", required = true)
        protected String userEmailAddress;
        @XmlElement(name = "Ownership", required = true)
        protected String ownership;
        @XmlElement(name = "PlatformId", required = true)
        protected DeviceSearchResult.Devices.PlatformId platformId;
        @XmlElement(name = "Platform", required = true)
        protected String platform;
        @XmlElement(name = "ModelId", required = true)
        protected DeviceSearchResult.Devices.ModelId modelId;
        @XmlElement(name = "Model", required = true)
        protected String model;
        @XmlElement(name = "OperatingSystem", required = true)
        protected String operatingSystem;
        @XmlElement(name = "PhoneNumber", required = true)
        protected String phoneNumber;
        @XmlElement(name = "LastSeen", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastSeen;
        @XmlElement(name = "EnrollmentStatus", required = true)
        protected String enrollmentStatus;
        @XmlElement(name = "ComplianceStatus", required = true)
        protected String complianceStatus;
        @XmlElement(name = "CompromisedStatus")
        protected boolean compromisedStatus;
        @XmlElement(name = "LastEnrolledOn", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastEnrolledOn;
        @XmlElement(name = "LastComplianceCheckOn", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastComplianceCheckOn;
        @XmlElement(name = "LastCompromisedCheckOn", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar lastCompromisedCheckOn;
        @XmlElement(name = "IsSupervised")
        protected boolean isSupervised;
        @XmlElement(name = "DeviceMCC", required = true)
        protected DeviceSearchResult.Devices.DeviceMCC deviceMCC;
        @XmlElement(name = "VirtualMemory")
        @XmlSchemaType(name = "unsignedByte")
        protected short virtualMemory;
        @XmlElement(name = "DeviceCapacity", required = true, nillable = true)
        protected String deviceCapacity;
        @XmlElement(name = "AvailableDeviceCapacity", required = true, nillable = true)
        protected String availableDeviceCapacity;
        @XmlElement(name = "LastSystemSampleTime", required = true, nillable = true)
        protected Object lastSystemSampleTime;
        @XmlElement(name = "IsDeviceDNDEnabled")
        protected boolean isDeviceDNDEnabled;
        @XmlElement(name = "IsDeviceLocatorEnabled")
        protected boolean isDeviceLocatorEnabled;
        @XmlElement(name = "IsCloudBackupEnabled")
        protected boolean isCloudBackupEnabled;
        @XmlElement(name = "IsActivationLockEnabled")
        protected boolean isActivationLockEnabled;
        @XmlElement(name = "IsNetworkTethered")
        protected boolean isNetworkTethered;
        @XmlElement(name = "BatteryLevel", required = true)
        protected String batteryLevel;
        @XmlElement(name = "IsRoaming")
        protected boolean isRoaming;
        @XmlElement(name = "LastNetworkLANSampleTime", required = true, nillable = true)
        protected Object lastNetworkLANSampleTime;
        @XmlElement(name = "LastBluetoothSampleTime", required = true, nillable = true)
        protected Object lastBluetoothSampleTime;
        @XmlElement(name = "SystemIntegrityProtectionEnabled")
        protected boolean systemIntegrityProtectionEnabled;
        @XmlElement(name = "ProcessorArchitecture")
        @XmlSchemaType(name = "unsignedByte")
        protected short processorArchitecture;
        @XmlElement(name = "UserApprovedEnrollment", required = true, nillable = true)
        protected Object userApprovedEnrollment;
        @XmlElement(name = "EnrolledViaDEP", required = true, nillable = true)
        protected Object enrolledViaDEP;
        @XmlElement(name = "TotalPhysicalMemory")
        @XmlSchemaType(name = "unsignedByte")
        protected short totalPhysicalMemory;
        @XmlElement(name = "AvailablePhysicalMemory")
        @XmlSchemaType(name = "unsignedByte")
        protected short availablePhysicalMemory;
        @XmlElement(name = "SecurityPatchDate", required = true, nillable = true)
        protected Object securityPatchDate;
        @XmlElement(name = "SystemUpdateReceivedTime", required = true, nillable = true)
        protected Object systemUpdateReceivedTime;
        @XmlElement(name = "IsSecurityPatchUpdate", required = true, nillable = true)
        protected Object isSecurityPatchUpdate;
        @XmlElement(name = "EasIds", required = true)
        protected DeviceSearchResult.Devices.EasIds easIds;
        
        // additional field as indicator for processing
        @XmlElement(name = "processed", required = true)
        protected boolean processed = false;

        public boolean isProcessed() {
			return processed;
		}

		public void setProcessed(boolean processed) {
			this.processed = processed;
		}

		/**
         * Ruft den Wert der id-Eigenschaft ab.
         * 
         */
        public int getId() {
            return id;
        }

        /**
         * Legt den Wert der id-Eigenschaft fest.
         * 
         */
        public void setId(int value) {
            this.id = value;
        }

        /**
         * Ruft den Wert der uuid-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUuid() {
            return uuid;
        }

        /**
         * Legt den Wert der uuid-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUuid(String value) {
            this.uuid = value;
        }

        /**
         * Ruft den Wert der udid-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUdid() {
            return udid;
        }

        /**
         * Legt den Wert der udid-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUdid(String value) {
            this.udid = value;
        }

        /**
         * Ruft den Wert der serialNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSerialNumber() {
            return serialNumber;
        }

        /**
         * Legt den Wert der serialNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSerialNumber(String value) {
            this.serialNumber = value;
        }

        /**
         * Ruft den Wert der macAddress-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMacAddress() {
            return macAddress;
        }

        /**
         * Legt den Wert der macAddress-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMacAddress(String value) {
            this.macAddress = value;
        }

        /**
         * Ruft den Wert der imei-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getImei() {
            return imei;
        }

        /**
         * Legt den Wert der imei-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setImei(BigInteger value) {
            this.imei = value;
        }

        /**
         * Ruft den Wert der assetNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssetNumber() {
            return assetNumber;
        }

        /**
         * Legt den Wert der assetNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssetNumber(String value) {
            this.assetNumber = value;
        }

        /**
         * Ruft den Wert der deviceFriendlyName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeviceFriendlyName() {
            return deviceFriendlyName;
        }

        /**
         * Legt den Wert der deviceFriendlyName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeviceFriendlyName(String value) {
            this.deviceFriendlyName = value;
        }

        /**
         * Ruft den Wert der locationGroupId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.LocationGroupId }
         *     
         */
        public DeviceSearchResult.Devices.LocationGroupId getLocationGroupId() {
            return locationGroupId;
        }

        /**
         * Legt den Wert der locationGroupId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.LocationGroupId }
         *     
         */
        public void setLocationGroupId(DeviceSearchResult.Devices.LocationGroupId value) {
            this.locationGroupId = value;
        }

        /**
         * Ruft den Wert der locationGroupName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocationGroupName() {
            return locationGroupName;
        }

        /**
         * Legt den Wert der locationGroupName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocationGroupName(String value) {
            this.locationGroupName = value;
        }

        /**
         * Ruft den Wert der userId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.UserId }
         *     
         */
        public DeviceSearchResult.Devices.UserId getUserId() {
            return userId;
        }

        /**
         * Legt den Wert der userId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.UserId }
         *     
         */
        public void setUserId(DeviceSearchResult.Devices.UserId value) {
            this.userId = value;
        }

        /**
         * Ruft den Wert der userName-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserName() {
            return userName;
        }

        /**
         * Legt den Wert der userName-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserName(String value) {
            this.userName = value;
        }

        /**
         * Ruft den Wert der userEmailAddress-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserEmailAddress() {
            return userEmailAddress;
        }

        /**
         * Legt den Wert der userEmailAddress-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserEmailAddress(String value) {
            this.userEmailAddress = value;
        }

        /**
         * Ruft den Wert der ownership-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnership() {
            return ownership;
        }

        /**
         * Legt den Wert der ownership-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnership(String value) {
            this.ownership = value;
        }

        /**
         * Ruft den Wert der platformId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.PlatformId }
         *     
         */
        public DeviceSearchResult.Devices.PlatformId getPlatformId() {
            return platformId;
        }

        /**
         * Legt den Wert der platformId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.PlatformId }
         *     
         */
        public void setPlatformId(DeviceSearchResult.Devices.PlatformId value) {
            this.platformId = value;
        }

        /**
         * Ruft den Wert der platform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlatform() {
            return platform;
        }

        /**
         * Legt den Wert der platform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlatform(String value) {
            this.platform = value;
        }

        /**
         * Ruft den Wert der modelId-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.ModelId }
         *     
         */
        public DeviceSearchResult.Devices.ModelId getModelId() {
            return modelId;
        }

        /**
         * Legt den Wert der modelId-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.ModelId }
         *     
         */
        public void setModelId(DeviceSearchResult.Devices.ModelId value) {
            this.modelId = value;
        }

        /**
         * Ruft den Wert der model-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModel() {
            return model;
        }

        /**
         * Legt den Wert der model-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModel(String value) {
            this.model = value;
        }

        /**
         * Ruft den Wert der operatingSystem-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperatingSystem() {
            return operatingSystem;
        }

        /**
         * Legt den Wert der operatingSystem-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperatingSystem(String value) {
            this.operatingSystem = value;
        }

        /**
         * Ruft den Wert der phoneNumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPhoneNumber() {
            return phoneNumber;
        }

        /**
         * Legt den Wert der phoneNumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPhoneNumber(String value) {
            this.phoneNumber = value;
        }

        /**
         * Ruft den Wert der lastSeen-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastSeen() {
            return lastSeen;
        }

        /**
         * Legt den Wert der lastSeen-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastSeen(XMLGregorianCalendar value) {
            this.lastSeen = value;
        }

        /**
         * Ruft den Wert der enrollmentStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEnrollmentStatus() {
            return enrollmentStatus;
        }

        /**
         * Legt den Wert der enrollmentStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEnrollmentStatus(String value) {
            this.enrollmentStatus = value;
        }

        /**
         * Ruft den Wert der complianceStatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComplianceStatus() {
            return complianceStatus;
        }

        /**
         * Legt den Wert der complianceStatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComplianceStatus(String value) {
            this.complianceStatus = value;
        }

        /**
         * Ruft den Wert der compromisedStatus-Eigenschaft ab.
         * 
         */
        public boolean isCompromisedStatus() {
            return compromisedStatus;
        }

        /**
         * Legt den Wert der compromisedStatus-Eigenschaft fest.
         * 
         */
        public void setCompromisedStatus(boolean value) {
            this.compromisedStatus = value;
        }

        /**
         * Ruft den Wert der lastEnrolledOn-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastEnrolledOn() {
            return lastEnrolledOn;
        }

        /**
         * Legt den Wert der lastEnrolledOn-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastEnrolledOn(XMLGregorianCalendar value) {
            this.lastEnrolledOn = value;
        }

        /**
         * Ruft den Wert der lastComplianceCheckOn-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastComplianceCheckOn() {
            return lastComplianceCheckOn;
        }

        /**
         * Legt den Wert der lastComplianceCheckOn-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastComplianceCheckOn(XMLGregorianCalendar value) {
            this.lastComplianceCheckOn = value;
        }

        /**
         * Ruft den Wert der lastCompromisedCheckOn-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLastCompromisedCheckOn() {
            return lastCompromisedCheckOn;
        }

        /**
         * Legt den Wert der lastCompromisedCheckOn-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLastCompromisedCheckOn(XMLGregorianCalendar value) {
            this.lastCompromisedCheckOn = value;
        }

        /**
         * Ruft den Wert der isSupervised-Eigenschaft ab.
         * 
         */
        public boolean isIsSupervised() {
            return isSupervised;
        }

        /**
         * Legt den Wert der isSupervised-Eigenschaft fest.
         * 
         */
        public void setIsSupervised(boolean value) {
            this.isSupervised = value;
        }

        /**
         * Ruft den Wert der deviceMCC-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.DeviceMCC }
         *     
         */
        public DeviceSearchResult.Devices.DeviceMCC getDeviceMCC() {
            return deviceMCC;
        }

        /**
         * Legt den Wert der deviceMCC-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.DeviceMCC }
         *     
         */
        public void setDeviceMCC(DeviceSearchResult.Devices.DeviceMCC value) {
            this.deviceMCC = value;
        }

        /**
         * Ruft den Wert der virtualMemory-Eigenschaft ab.
         * 
         */
        public short getVirtualMemory() {
            return virtualMemory;
        }

        /**
         * Legt den Wert der virtualMemory-Eigenschaft fest.
         * 
         */
        public void setVirtualMemory(short value) {
            this.virtualMemory = value;
        }

        /**
         * Ruft den Wert der deviceCapacity-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeviceCapacity() {
            return deviceCapacity;
        }

        /**
         * Legt den Wert der deviceCapacity-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeviceCapacity(String value) {
            this.deviceCapacity = value;
        }

        /**
         * Ruft den Wert der availableDeviceCapacity-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAvailableDeviceCapacity() {
            return availableDeviceCapacity;
        }

        /**
         * Legt den Wert der availableDeviceCapacity-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAvailableDeviceCapacity(String value) {
            this.availableDeviceCapacity = value;
        }

        /**
         * Ruft den Wert der lastSystemSampleTime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLastSystemSampleTime() {
            return lastSystemSampleTime;
        }

        /**
         * Legt den Wert der lastSystemSampleTime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLastSystemSampleTime(Object value) {
            this.lastSystemSampleTime = value;
        }

        /**
         * Ruft den Wert der isDeviceDNDEnabled-Eigenschaft ab.
         * 
         */
        public boolean isIsDeviceDNDEnabled() {
            return isDeviceDNDEnabled;
        }

        /**
         * Legt den Wert der isDeviceDNDEnabled-Eigenschaft fest.
         * 
         */
        public void setIsDeviceDNDEnabled(boolean value) {
            this.isDeviceDNDEnabled = value;
        }

        /**
         * Ruft den Wert der isDeviceLocatorEnabled-Eigenschaft ab.
         * 
         */
        public boolean isIsDeviceLocatorEnabled() {
            return isDeviceLocatorEnabled;
        }

        /**
         * Legt den Wert der isDeviceLocatorEnabled-Eigenschaft fest.
         * 
         */
        public void setIsDeviceLocatorEnabled(boolean value) {
            this.isDeviceLocatorEnabled = value;
        }

        /**
         * Ruft den Wert der isCloudBackupEnabled-Eigenschaft ab.
         * 
         */
        public boolean isIsCloudBackupEnabled() {
            return isCloudBackupEnabled;
        }

        /**
         * Legt den Wert der isCloudBackupEnabled-Eigenschaft fest.
         * 
         */
        public void setIsCloudBackupEnabled(boolean value) {
            this.isCloudBackupEnabled = value;
        }

        /**
         * Ruft den Wert der isActivationLockEnabled-Eigenschaft ab.
         * 
         */
        public boolean isIsActivationLockEnabled() {
            return isActivationLockEnabled;
        }

        /**
         * Legt den Wert der isActivationLockEnabled-Eigenschaft fest.
         * 
         */
        public void setIsActivationLockEnabled(boolean value) {
            this.isActivationLockEnabled = value;
        }

        /**
         * Ruft den Wert der isNetworkTethered-Eigenschaft ab.
         * 
         */
        public boolean isIsNetworkTethered() {
            return isNetworkTethered;
        }

        /**
         * Legt den Wert der isNetworkTethered-Eigenschaft fest.
         * 
         */
        public void setIsNetworkTethered(boolean value) {
            this.isNetworkTethered = value;
        }

        /**
         * Ruft den Wert der batteryLevel-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBatteryLevel() {
            return batteryLevel;
        }

        /**
         * Legt den Wert der batteryLevel-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBatteryLevel(String value) {
            this.batteryLevel = value;
        }

        /**
         * Ruft den Wert der isRoaming-Eigenschaft ab.
         * 
         */
        public boolean isIsRoaming() {
            return isRoaming;
        }

        /**
         * Legt den Wert der isRoaming-Eigenschaft fest.
         * 
         */
        public void setIsRoaming(boolean value) {
            this.isRoaming = value;
        }

        /**
         * Ruft den Wert der lastNetworkLANSampleTime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLastNetworkLANSampleTime() {
            return lastNetworkLANSampleTime;
        }

        /**
         * Legt den Wert der lastNetworkLANSampleTime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLastNetworkLANSampleTime(Object value) {
            this.lastNetworkLANSampleTime = value;
        }

        /**
         * Ruft den Wert der lastBluetoothSampleTime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getLastBluetoothSampleTime() {
            return lastBluetoothSampleTime;
        }

        /**
         * Legt den Wert der lastBluetoothSampleTime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setLastBluetoothSampleTime(Object value) {
            this.lastBluetoothSampleTime = value;
        }

        /**
         * Ruft den Wert der systemIntegrityProtectionEnabled-Eigenschaft ab.
         * 
         */
        public boolean isSystemIntegrityProtectionEnabled() {
            return systemIntegrityProtectionEnabled;
        }

        /**
         * Legt den Wert der systemIntegrityProtectionEnabled-Eigenschaft fest.
         * 
         */
        public void setSystemIntegrityProtectionEnabled(boolean value) {
            this.systemIntegrityProtectionEnabled = value;
        }

        /**
         * Ruft den Wert der processorArchitecture-Eigenschaft ab.
         * 
         */
        public short getProcessorArchitecture() {
            return processorArchitecture;
        }

        /**
         * Legt den Wert der processorArchitecture-Eigenschaft fest.
         * 
         */
        public void setProcessorArchitecture(short value) {
            this.processorArchitecture = value;
        }

        /**
         * Ruft den Wert der userApprovedEnrollment-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getUserApprovedEnrollment() {
            return userApprovedEnrollment;
        }

        /**
         * Legt den Wert der userApprovedEnrollment-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setUserApprovedEnrollment(Object value) {
            this.userApprovedEnrollment = value;
        }

        /**
         * Ruft den Wert der enrolledViaDEP-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getEnrolledViaDEP() {
            return enrolledViaDEP;
        }

        /**
         * Legt den Wert der enrolledViaDEP-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setEnrolledViaDEP(Object value) {
            this.enrolledViaDEP = value;
        }

        /**
         * Ruft den Wert der totalPhysicalMemory-Eigenschaft ab.
         * 
         */
        public short getTotalPhysicalMemory() {
            return totalPhysicalMemory;
        }

        /**
         * Legt den Wert der totalPhysicalMemory-Eigenschaft fest.
         * 
         */
        public void setTotalPhysicalMemory(short value) {
            this.totalPhysicalMemory = value;
        }

        /**
         * Ruft den Wert der availablePhysicalMemory-Eigenschaft ab.
         * 
         */
        public short getAvailablePhysicalMemory() {
            return availablePhysicalMemory;
        }

        /**
         * Legt den Wert der availablePhysicalMemory-Eigenschaft fest.
         * 
         */
        public void setAvailablePhysicalMemory(short value) {
            this.availablePhysicalMemory = value;
        }

        /**
         * Ruft den Wert der securityPatchDate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSecurityPatchDate() {
            return securityPatchDate;
        }

        /**
         * Legt den Wert der securityPatchDate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSecurityPatchDate(Object value) {
            this.securityPatchDate = value;
        }

        /**
         * Ruft den Wert der systemUpdateReceivedTime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getSystemUpdateReceivedTime() {
            return systemUpdateReceivedTime;
        }

        /**
         * Legt den Wert der systemUpdateReceivedTime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setSystemUpdateReceivedTime(Object value) {
            this.systemUpdateReceivedTime = value;
        }

        /**
         * Ruft den Wert der isSecurityPatchUpdate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getIsSecurityPatchUpdate() {
            return isSecurityPatchUpdate;
        }

        /**
         * Legt den Wert der isSecurityPatchUpdate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setIsSecurityPatchUpdate(Object value) {
            this.isSecurityPatchUpdate = value;
        }

        /**
         * Ruft den Wert der easIds-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DeviceSearchResult.Devices.EasIds }
         *     
         */
        public DeviceSearchResult.Devices.EasIds getEasIds() {
            return easIds;
        }

        /**
         * Legt den Wert der easIds-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DeviceSearchResult.Devices.EasIds }
         *     
         */
        public void setEasIds(DeviceSearchResult.Devices.EasIds value) {
            this.easIds = value;
        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SIMMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrentMCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "simmcc",
            "currentMCC"
        })
        public static class DeviceMCC {

            @XmlElement(name = "SIMMCC", required = true)
            protected String simmcc;
            @XmlElement(name = "CurrentMCC", required = true)
            protected String currentMCC;

            /**
             * Ruft den Wert der simmcc-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSIMMCC() {
                return simmcc;
            }

            /**
             * Legt den Wert der simmcc-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSIMMCC(String value) {
                this.simmcc = value;
            }

            /**
             * Ruft den Wert der currentMCC-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrentMCC() {
                return currentMCC;
            }

            /**
             * Legt den Wert der currentMCC-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrentMCC(String value) {
                this.currentMCC = value;
            }

        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="EasId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "easId"
        })
        public static class EasIds {

            @XmlElement(name = "EasId", required = true)
            protected String easId;

            /**
             * Ruft den Wert der easId-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEasId() {
                return easId;
            }

            /**
             * Legt den Wert der easId-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEasId(String value) {
                this.easId = value;
            }

        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedShort">
         *       &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class LocationGroupId {

            @XmlValue
            @XmlSchemaType(name = "unsignedShort")
            protected int value;
            @XmlAttribute(name = "uuid", required = true)
            protected String uuid;
            @XmlAttribute(name = "title", required = true)
            protected String title;

            /**
             * Ruft den Wert der value-Eigenschaft ab.
             * 
             */
            public int getValue() {
                return value;
            }

            /**
             * Legt den Wert der value-Eigenschaft fest.
             * 
             */
            public void setValue(int value) {
                this.value = value;
            }

            /**
             * Ruft den Wert der uuid-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUuid() {
                return uuid;
            }

            /**
             * Legt den Wert der uuid-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUuid(String value) {
                this.uuid = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
         *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class ModelId {

            @XmlValue
            @XmlSchemaType(name = "unsignedByte")
            protected short value;
            @XmlAttribute(name = "title", required = true)
            protected String title;

            /**
             * Ruft den Wert der value-Eigenschaft ab.
             * 
             */
            public short getValue() {
                return value;
            }

            /**
             * Legt den Wert der value-Eigenschaft fest.
             * 
             */
            public void setValue(short value) {
                this.value = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedByte">
         *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class PlatformId {

            @XmlValue
            @XmlSchemaType(name = "unsignedByte")
            protected short value;
            @XmlAttribute(name = "title", required = true)
            protected String title;

            /**
             * Ruft den Wert der value-Eigenschaft ab.
             * 
             */
            public short getValue() {
                return value;
            }

            /**
             * Legt den Wert der value-Eigenschaft fest.
             * 
             */
            public void setValue(short value) {
                this.value = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

        }


        /**
         * <p>Java-Klasse fuer anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>unsignedInt">
         *       &lt;attribute name="uuid" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="title" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class UserId {

            @XmlValue
            @XmlSchemaType(name = "unsignedInt")
            protected long value;
            @XmlAttribute(name = "uuid", required = true)
            protected String uuid;
            @XmlAttribute(name = "title", required = true)
            protected String title;

            /**
             * Ruft den Wert der value-Eigenschaft ab.
             * 
             */
            public long getValue() {
                return value;
            }

            /**
             * Legt den Wert der value-Eigenschaft fest.
             * 
             */
            public void setValue(long value) {
                this.value = value;
            }

            /**
             * Ruft den Wert der uuid-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUuid() {
                return uuid;
            }

            /**
             * Legt den Wert der uuid-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUuid(String value) {
                this.uuid = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

        }

    }

}
