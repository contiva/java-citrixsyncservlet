//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Aenderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.03.27 um 03:48:15 PM CET 
//


package com.air_watch.servicemodel.resources;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.air_watch.servicemodel.resources package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.air_watch.servicemodel.resources
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeviceSearchResult }
     * 
     */
    public DeviceSearchResult createDeviceSearchResult() {
        return new DeviceSearchResult();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices }
     * 
     */
    public DeviceSearchResult.Devices createDeviceSearchResultDevices() {
        return new DeviceSearchResult.Devices();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.LocationGroupId }
     * 
     */
    public DeviceSearchResult.Devices.LocationGroupId createDeviceSearchResultDevicesLocationGroupId() {
        return new DeviceSearchResult.Devices.LocationGroupId();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.UserId }
     * 
     */
    public DeviceSearchResult.Devices.UserId createDeviceSearchResultDevicesUserId() {
        return new DeviceSearchResult.Devices.UserId();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.PlatformId }
     * 
     */
    public DeviceSearchResult.Devices.PlatformId createDeviceSearchResultDevicesPlatformId() {
        return new DeviceSearchResult.Devices.PlatformId();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.ModelId }
     * 
     */
    public DeviceSearchResult.Devices.ModelId createDeviceSearchResultDevicesModelId() {
        return new DeviceSearchResult.Devices.ModelId();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.DeviceMCC }
     * 
     */
    public DeviceSearchResult.Devices.DeviceMCC createDeviceSearchResultDevicesDeviceMCC() {
        return new DeviceSearchResult.Devices.DeviceMCC();
    }

    /**
     * Create an instance of {@link DeviceSearchResult.Devices.EasIds }
     * 
     */
    public DeviceSearchResult.Devices.EasIds createDeviceSearchResultDevicesEasIds() {
        return new DeviceSearchResult.Devices.EasIds();
    }

}
