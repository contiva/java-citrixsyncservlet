package de.peri.sap.po.ejb.citrix.sync;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.peri.sap.po.ejb.citrix.sync.JAXBHelper;

import com.air_watch.servicemodel.resources.DeviceSearchResult;
import com.cisco.ise.ers.identity.Endpoint;
import com.cisco.ise.ers.identity.ObjectFactory;

/**
 * Die Klasse IseDevice implementiert alle Eigenschaften und Methoden sowie eine Liste der ISE Geräte
 * @author Sven
 *
 */
public class IseDevice {

	// objects for http request
	private HttpRequest httpReq;
	private HashMap<String, String> headers = new HashMap<String, String>();
	private static final String groupIdUri = "ers/config/endpointgroup?filter=name.EQ.";
	private static final String operateEndpoint = "ers/config/endpoint/";
	private String getUri = "https://%s:%d/ers/config/endpoint?size=%d&page=%d&filter=groupId.EQ.%s";
	private int Total = 1; 																					// init to 1, to get number of devices on first http call
	private int page = 1;	
	private int size = 100;

	// Ise properties
	private String localhost;
	private String groupID;
	private String groupName;
	private String User;
	private String Password;
	private int port;

	// list of Ise devices
	public List<IseDevices> devices = null;
	
	// logger
	private Logger log;

	// getter and setter for private objects
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getLocalhost() {
		return localhost;
	}

	public void setLocalhost(String localhost) {
		this.localhost = localhost;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUser() {
		return User;
	}

	public void setUser(String user) {
		User = user;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	@SuppressWarnings("unused")
	private IseDevice() {
		// private default constructor to prevent from initializing an empty
		// IseDevice
	}

	/**
	 * Im Konstruktor werden alle Eigenschaften initiiert, die Gruppen ID zum Gruppennamen ausgelesen
	 * und die Liste der Geräte geladen
	 * 
	 * @param host								DNS Name oder IP Adresse
	 * @param port								Portnummer
	 * @param user								Benutzer
	 * @param pwd								Passwort
	 * @param groupName							Gruppenname der ISE Gruppe, die abgeglichen werden soll
	 * @param log								Logger
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public IseDevice(String host, int port, String user, String pwd, String groupName, Logger log)
			throws IOException, ParserConfigurationException, SAXException {

		devices = new ArrayList<IseDevices>();

		this.log = log;

		httpReq = new HttpRequest(log);

		setLocalhost(host);
		setPort(port);
		setUser(user);
		setPassword(pwd);
		setGroupName(groupName);

		setHeaders();

		setGroupID(getIdForGroupFromIse(this.getGroupName()));
		this.devices = getAllDevicesFromIse();
	}

	/**
	 * Gibt die Id der übergebenen Gruppe zurück
	 * API Call ISE Rest API 
	 * 
	 * @param group							Gruppenname der ISE Gruppe
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	private String getIdForGroupFromIse(String group) throws IOException, ParserConfigurationException, SAXException {

		String uri = "https://" + localhost + ":" + port + "/" + groupIdUri + group;

		InputStream is;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		is = httpReq.getHttpsResponseWithNoValidation(uri, headers, "GET", getUser(), getPassword());

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		Node resource = doc.getFirstChild().getChildNodes().item(0).getChildNodes().item(0);

		if (resource.hasAttributes()) {
			NamedNodeMap attributes = resource.getAttributes();
			groupID = attributes.getNamedItem("id").getNodeValue();
		}

		return groupID;
	}

	/**
	 * Gibt die Liste der ISE Geräte zurück. Abfrage über API Calls, die seitenweise Ergebnisse liefern
	 * API Call ISE Rest API 
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public List<IseDevices> getAllDevicesFromIse() throws IOException, ParserConfigurationException, SAXException {

		// init to first page
		String url = String.format(getUri, localhost, port, size, page, groupID);
		overwriteAcceptForGet();

		InputStream is;

		// first http call retrieves number of devices
		is = httpReq.getHttpsResponseWithNoValidation(url, headers, "GET", getUser(), getPassword());

		// get complete number of devices
		Total = parseIsePageForNumber(is);
		log.info(Total + " number of IseDevices found.");
		if (Total > 0) {

			int numberOfPages = 0;

			// is the chunk size (default 100) bigger than total, only get one
			// chunk
			if (Total < size) {
				size = Total;
			}

			if (Total % size > 0) {
				numberOfPages = Total / size + 1;
			} else {
				numberOfPages = Total / size;
			}

			while (page <= numberOfPages) {
				if (page == numberOfPages) {
					size = Total % size;
				}
				url = String.format(getUri, localhost, port, size, page, groupID);
				log.info("Seite:" + page);

				// get all devices
				is = httpReq.getHttpsResponseWithNoValidation(url, headers, "GET", getUser(), getPassword());

				parseIsePage(is);

				page += 1;
			}
		}

		return devices;
	}

	/**
	 * Löscht ein Gerät aus der ISE
	 * API Call ISE Rest API 
	 * @param device
	 * @throws IOException
	 */
	public void deleteIseDevice(IseDevices device) throws IOException {

		String url = "https://" + localhost + ":" + port + "/" + operateEndpoint + device.getId();
		overwriteAcceptForDelete();

		httpReq.getHttpsResponseWithNoValidation(url, headers, "DELETE", getUser(), getPassword());
	}

	/**
	 * Fügt ein Gerät der ISE hinzu
	 * API Call ISE Rest API 
	 * @param device
	 */
	public void addIseDevice(DeviceSearchResult.Devices device) {

		String url = "https://" + localhost + ":" + port + "/" + operateEndpoint;
		overwriteAcceptForAdd();

		try {
			String content = createHttpPostContent(device);
			log.info("Geraet mit folgender MAC  hinzufuegen: " + device.getMacAddress());

			int respCode = httpReq.getHttpsResponseFromPostWithNoValidation(url, headers, "POST", getUser(),
					getPassword(), content);
			log.info("ResponseCode: " + respCode);

			if (respCode != 200) {
				// Device already exists, but there is no way to do an update
				// just log the info
				log.info("Geraet existiert in einer anderen Gruppe, Aktualisierung nicht moeglich");

			}

		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (JAXBException e1) {
			e1.printStackTrace();
			log.error(e1.getMessage());
		}
	}

	/**
	 * Erzeugt den Body als Javaobject und marshaled diesen dann nach XML für den Create API CALL 
	 * @param device
	 * @return
	 * @throws JAXBException
	 */
	public String createHttpPostContent(DeviceSearchResult.Devices device) throws JAXBException {

		com.cisco.ise.ers.identity.ObjectFactory identityFactory = new ObjectFactory();
		JAXBHelper jaxbh = new JAXBHelper(identityFactory.getClass().getPackage().getName());

		Endpoint endpoint = new Endpoint();

		endpoint.setName(device.getMacAddress());
		endpoint.setMac(device.getMacAddress());
		endpoint.setPortalUser(device.getUserName() + "@corp.peri");
		endpoint.setDescription("Automatically created through AirWatch Sync - phone: " + device.getPhoneNumber());
		endpoint.setGroupId(groupID);
		endpoint.setStaticGroupAssignment(true);
		endpoint.setStaticProfileAssignment(false);

		JAXBElement<Endpoint> jaxbEndPoint = identityFactory.createEndpoint(endpoint);

		String endpointAsXml = jaxbh.marshal(jaxbEndPoint);

		return endpointAsXml;
	}

	/**
	 * Durchsucht die 1. Response nach der Anzahl an Geräten, um das seitenweise Abrufen zu ermöglichen 
	 * @param is							Response Stream vom ISE
	 * @return								Anzahl der Geräte
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private int parseIsePageForNumber(InputStream is) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		Node rootNode = doc.getFirstChild();

		if (rootNode.hasAttributes()) {
			NamedNodeMap attributes = rootNode.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++) {

				if (attributes.item(i).getNodeName().equals("total")) {
					Total = Integer.parseInt(attributes.item(i).getTextContent());
				}
			}
		}

		return Total;

	}

	/**
	 * Verarbeitet den übergebene XML Stream und fügt ISE Geräte der Objekt Liste hinzu  
	 * @param is								Response Stream vom ISE
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void parseIsePage(InputStream is) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db;

		db = dbf.newDocumentBuilder();
		Document doc;
		doc = db.parse(is);

		NodeList nodes = doc.getElementsByTagNameNS("*", "resource");

		for (int i = 0; i < nodes.getLength(); i++) {
			IseDevices newDevice = new IseDevices();
			if (nodes.item(i).hasAttributes()) {
				NamedNodeMap attributes = nodes.item(i).getAttributes();
				for (int j = 0; j < attributes.getLength(); j++) {
					if (attributes.item(j).getNodeName().equals("name")) {
						newDevice.setMacAddress(attributes.item(j).getTextContent());
					}
					if (attributes.item(j).getNodeName().equals("id")) {
						newDevice.setId(attributes.item(j).getTextContent());
					}
				}
				devices.add(newDevice);
			}
		}

	}

	/**
	 * Setzt die Standard Header für den intiialen API Call zur Gruppen ID Ermittlung
	 */
	private void setHeaders() {

		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpointgroup.1.0+xml");
		headers.put("Accept-Search-Result", "application/vnd.com.cisco.ise.ers.searchresult.2.0+xml");
		headers.put("Encoding", "UTF-8");
		headers.put("Content-Type", "application/vnd.com.cisco.ise.ers.ersresponse.1.1+xml;charset=utf-8");

	}

	/**
	 * Setzt den HTTP Request Header f�r das Auslesen
	 */
	private void overwriteAcceptForGet() {
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.0+xml");
	}
	/**
	 * Setzt den HTTP Request Header f�r das L�schen
	 */
	private void overwriteAcceptForDelete() {
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml");
	}
	/**
	 * Setzt den HTTP Request Header f�r das Hinzuf�gen
	 */
	private void overwriteAcceptForAdd() {
		headers.put("Content-Type", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml; charset=utf-8");
		headers.put("Accept", "application/vnd.com.cisco.ise.identity.endpoint.1.1+xml");
	}

}
