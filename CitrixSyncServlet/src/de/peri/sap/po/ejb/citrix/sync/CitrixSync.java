package de.peri.sap.po.ejb.citrix.sync;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
//import java.util.logging.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.xml.sax.SAXException;



/**
 * Die Klasse CitrixSync implementiert die komplette Business Logik
 * 
 * Im Konstruktor werden die Klassen für Airwatch und ISE instanziert und direkt die Gerätelisten erstellt.
 * Anschließend wird in 2 Schleifen der Abgleich durchge- und die jeweils notwendig Aktion ausgeführt. 
 * @author Sven Brandt
 *
 */


/** 
 * Konstruktor
 * @author Sven
 *
 */
public class CitrixSync {

	MdMDevice mdmDevice;
	IseDevice iseDevice;

	String Log = "";
	private Logger log;

	/**
	 * Default constructor.
	 */
	@SuppressWarnings("unused")
	private CitrixSync() {
	}

	public CitrixSync(String iseHost, int isePort, String iseUser, String isePwd,String iseGroupName,  String mdmHost, String mdmUser,
			String mdmPwd, Logger log)
			throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException {
		
		this.log = log;

				
		// create MDMDevice List and populate it via http call
		log.info("MDM Device Liste laden");
		mdmDevice = new MdMDevice(mdmHost, mdmUser, mdmPwd, log);
		// create ISEDevice List and populate it via rest api call
//		log.info("ISE Device Liste laden");
//		iseDevice = new IseDevice(iseHost, isePort, iseUser, isePwd, iseGroupName,  log);
//		processSync();
	}

/**
 * Öffentliche Methode zur Ausführung der Business Logik
 * @throws IOException
 */
	public void processSync() throws IOException {

		// complete business logic is implemented in here

		// add or delete from iseDevice List
		log.info("Vergleich starten und ISE Liste aktualisieren.");
		compareAndProcessDevices(mdmDevice, iseDevice);

	}
	
/**
 * Eigentliche Business Logik
 * 
 * In der ersten Schleife wird die Liste der Airwatch Gerate durchgegangen und in einer 2. Schleife mit den MAC Adressen der Gerate in der ISE List verglichen.
 * Wenn das Gerat gefunden wurde, wird das entsprechende Gerat in der ISE Liste markiert und als gefunden markiert. Wird das Gerat nicht gefunden, wird es am Ende
 * der 1. Schleife neu angelegt.
 * In der 3. Schleife werden alle ISE-Gerate, die nicht markiert wurden, aus der ISE gelöscht.
 * 
 * @param mdmDevice	- Instanz der Klasse für die Airwatch Gerateliste
 * @param iseDevice - Instanz der Klasse für die ISE Gerateliste
 * @throws IOException
 */
	private void compareAndProcessDevices(MdMDevice mdmDevice, IseDevice iseDevice) throws IOException {

		// comparison between the different device lists is implemented here
		boolean found = false;

		// loop through MdMDevice List and check if MdmDevice MAC is already
		// contained in IseDevice List
		for (int i = 0; i < mdmDevice.devices.size(); i++) {
			for (int j = 0; j < iseDevice.devices.size(); j++) {
				if (mdmDevice.devices.get(i).getMacAddress()
						.equals(iseDevice.devices.get(j).getMacAddress().replaceAll(":", ""))) {
					found = true;
					iseDevice.devices.get(j).setProcessed(true);
					log.info("Device with MacAddress" + mdmDevice.devices.get(i).getMacAddress() + " found.");

				}
			}
			if (found != true) {
				// Mdm not found in ISE List --> create new Ise Endpoint
				log.info("Add Device with Mac: " + mdmDevice.devices.get(i).getMacAddress() + " to ISE system.");
				iseDevice.addIseDevice(mdmDevice.devices.get(i));
			}
			found = false;
		}

		// all iseDevices which are not processed are no longer valid and will
		// be deleted
		for (int j = 0; j < iseDevice.devices.size(); j++) {
			if (!iseDevice.devices.get(j).isProcessed()) {
				iseDevice.deleteIseDevice(iseDevice.devices.get(j));
				log.info(
						"Deleted Device with Mac: " + iseDevice.devices.get(j).getMacAddress() + " from ISE system.");
			}
		}

	}
/**
 * Main Methode zu Testzwecken --> in Zukunft besser mit JUnit4 testen
 * @param Aktuelle Run Configuration f�r Test
 * @param args 172.16.10.171 9060 API_Guest_Access 346xtwTaHuMjvhyJ$XbQY!K3Cuzr3N GuestEndpoints_MDM https://as1108.awmdm.com/api/mdm/devices/search  corp.peri\srvmdm2ise n_qx.ASzJHcMLa3$5ip96kmf!RBuDt
 */
	public static void main(String[] args) {

		final Logger log = LoggerFactory.getLogger(CitrixSync.class);
		
		try {
			@SuppressWarnings("unused")
			CitrixSync cs = new CitrixSync(args[0], Integer.parseInt(args[1]), args[2], args[3], args[4],
					 args[5], args[6], args[7],  log);
		} catch (Exception e) {

			log.info(e.getMessage());
			e.printStackTrace();
		}

	}

}
