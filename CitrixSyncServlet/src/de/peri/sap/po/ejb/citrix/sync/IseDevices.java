package de.peri.sap.po.ejb.citrix.sync;

import com.cisco.ise.ers.identity.Endpoint;

public class IseDevices extends Endpoint{
	
	private String macAddress;
	private String id;
	private boolean processed = false;
	
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isProcessed() {
		return processed;
	}
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
	
	
	

}
