package de.peri.sap.po.ejb.citrix.sync;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class myAuthenticator extends Authenticator {
		
	public myAuthenticator(String user, String pwd){
		setUser(user);
		setPwd(pwd);
	}
	
		private String user;
		private String pwd;

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getPwd() {
			return pwd;
		}

		public void setPwd(String pwd) {
			this.pwd = pwd;
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {				
			return new PasswordAuthentication(getUser(), getPwd().toCharArray());
		}
	}


