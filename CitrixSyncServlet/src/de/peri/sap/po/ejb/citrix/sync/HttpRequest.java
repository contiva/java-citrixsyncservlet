package de.peri.sap.po.ejb.citrix.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Hilfsklasse für die verschiedenen HTTP Aufrufe
 * 
 * @author Sven
 *
 */
public class HttpRequest {

	private Logger log;

	public HttpRequest(Logger log) {
		this.log = log;
	}

	/**
	 * Führt einen HTTP Request aus und gibt einen InputStream mit der Response
	 * zurück
	 * 
	 * @param urlString
	 *            aufzurufende URL
	 * @param headers
	 *            Key/Vaule pair f�r HTTP Header
	 * @param method
	 *            GET/POST
	 * @param user
	 *            Benutzer
	 * @param pwd
	 *            Passwort
	 * @return Response Input Stream
	 * @throws IOException
	 */
	public InputStream getHttpResponse(String urlString, HashMap<String, String> headers, String method, String user,
			String pwd) throws IOException {

		URL url;
		log.info("Request to URL: " + urlString);
		url = new URL(urlString);

		HttpURLConnection con = (HttpURLConnection) url.openConnection();

		con.setRequestMethod(method);
		con.setRequestProperty("Content-Type", "application/xml");

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}

		con.setDoOutput(true);
		myAuthenticator.setDefault(new myAuthenticator(user, pwd));

		InputStream ret = con.getInputStream();
		log.info("Response Code: " + con.getResponseCode());

		return ret;
	}


	public InputStream getHttpsResponse(String urlString, HashMap<String, String> headers, String method, String user,
			String pwd) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

		URL url;
		InputStream ret = null;
		log.info("Request to URL: " + urlString);


		 url = new URL(urlString);

		try {
			// Open a HTTP connection to the URL
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			
			con.setRequestMethod(method);
			con.setRequestProperty("Content-Type", "application/xml");

			String authStr = user + ":" + pwd;
			Encoder enc = Base64.getEncoder();
			String authEncoded = enc.encodeToString(authStr.getBytes());
			con.setRequestProperty("Authorization", "Basic " + authEncoded);

			for (Map.Entry<String, String> entry : headers.entrySet()) {
				con.setRequestProperty(entry.getKey(), entry.getValue());
			}

			// con.setDoInput(true);
			log.info("Getting InputStream");
			ret = con.getInputStream();
			log.info("Response Code: " + con.getResponseCode());

		} catch (Exception e) {
			log.error(e.getMessage());
			StackTraceElement[] stackTrace = e.getStackTrace();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < stackTrace.length; i++) {
				sb.append(stackTrace[i].toString());
				sb.append(System.getProperty("line.separator"));
			}
			log.error(sb.toString());
		}

		return ret;

	}

	/**
	 * Führt einen HTTPS Request (GET) aus der selbstsignierte Zertifikate
	 * akzeptiert (ISE) und die Response als InputStream zurückgibt
	 * 
	 * @param urlString
	 *            aufzurufende URL
	 * @param headers
	 *            HTTP Header
	 * @param method
	 *            POST/GET
	 * @param user
	 *            Benutzer
	 * @param pwd
	 *            Passwort
	 * @return Response Stream
	 * @throws MalformedURLException
	 */
	public InputStream getHttpsResponseWithNoValidation(String urlString, HashMap<String, String> headers,
			String method, String user, String pwd) throws MalformedURLException {

		URL url = new URL(urlString);
		log.info("Request to URL: " + urlString);

		InputStream is = null;

		// this part is necessary to accept self signed certificates
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

		} };

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		try {
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier validHosts = new HostnameVerifier() {

			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}

		};

		HttpsURLConnection.setDefaultHostnameVerifier(validHosts);

		try {
			// initiate connection but do not open it yet
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod(method);
			for (Map.Entry<String, String> entry : headers.entrySet()) {
				con.setRequestProperty(entry.getKey(), entry.getValue());
			}

			// set authentication
			myAuthenticator.setDefault(new myAuthenticator(user, pwd));

			is = con.getInputStream();
			log.info("Response Code: " + con.getResponseCode());
		} catch (ProtocolException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

		return is;

	}

	/**
	 * 
	 * 
	 * @param urlString
	 *            aufzurufende URL
	 * @param headers
	 *            HTTP Header
	 * @param method
	 *            POST/GET
	 * @param user
	 *            Benutzer
	 * @param pwd
	 *            Passwort
	 * @param content
	 *            Content Type
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public int getHttpsResponseFromPostWithNoValidation(String urlString, HashMap<String, String> headers,
			String method, String user, String pwd, String content) throws UnsupportedEncodingException, IOException {

		URL url = new URL(urlString);
		log.info("Request to URL: " + urlString);

		// this part is necessary to accept self signed certificates
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

		} };

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier validHosts = new HostnameVerifier() {

			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}

		};

		HttpsURLConnection.setDefaultHostnameVerifier(validHosts);

		// initiate connection but do not open it yet
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

		// set necessary header fields
		con.setRequestMethod(method);

		for (Map.Entry<String, String> entry : headers.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}

		con.setRequestMethod(method);
		con.setRequestProperty("Content-Length", String.valueOf(content.getBytes("UTF-8").length));
		con.setDoOutput(true);

		con.getOutputStream().write(content.getBytes("UTF-8"));

		// set authentication
		myAuthenticator.setDefault(new myAuthenticator(user, pwd));

		int responseCode = 0;

		try {
			// get response code and payload
			InputStream is = con.getInputStream();
			responseCode = con.getResponseCode();
			log.info("Response Code: " + responseCode);
			byte[] response = new byte[10000];

			while (is.read() > 0) {
				is.read(response);
				log.info(new String(response));
			}

		} catch (IOException e) {

			// something went wrong, get response code and error payload
			InputStream es = con.getErrorStream();

			byte[] errorResponse = new byte[10000];

			while (es.read() > 0) {
				responseCode = con.getResponseCode();
				es.read(errorResponse);
				log.error(new String(errorResponse));
			}
		}

		return responseCode;

	}

}