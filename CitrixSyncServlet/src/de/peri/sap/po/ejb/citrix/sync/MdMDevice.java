package de.peri.sap.po.ejb.citrix.sync;

import com.air_watch.servicemodel.resources.DeviceSearchResult;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.xml.sax.SAXException;

/**
 * Diese Klasse enthält alle Methoden zur Kommunikation mit dem Airwatch Dienst
 * und eine Liste der Geräte basierend auf der XML Struktur DeviceSearchResult
 * Die XSD für DeviceSearchResult ist mit Hilfe des Online XML to XSD Converters
 * XmlGrid.net http://xmlgrid.net erstellt worden
 * 
 * @author Sven Brandt
 *
 */
public class MdMDevice {

	// objects for http request
	HttpRequest httpReq;
	int MdmPage = 0;
	int MdmTotal = 9999;
	private static final int PAGESIZE = 500;

	// Airwatch properties
	private String host;
	private String user;
	private String pwd;
	public List<DeviceSearchResult.Devices> devices = null;

	// logger
	private Logger logger;

	// getter and setter for private objects
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * Default Konstruktor - ist private, damit wird verhindert, dass er benutzt
	 * werden kann.
	 */
	@SuppressWarnings("unused")
	private MdMDevice() {

	}

	/**
	 * Im Konstruktor werden alle Parameter der Klasse initiiert und die Liste der
	 * Geräte geladen
	 * 
	 * 
	 * @param host
	 *            URL zu Airwatch
	 * @param user
	 *            Benutzer
	 * @param pwd
	 *            Passwort
	 * @param log
	 *            Logger
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws JAXBException
	 * @throws XMLStreamException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws KeyManagementException
	 */
	public MdMDevice(String host, String user, String pwd, Logger log)
			throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException,
			KeyManagementException, KeyStoreException, NoSuchAlgorithmException {

		this.logger = log;

		httpReq = new HttpRequest(log);

		log.info("Airwatch URL: " + host);
		log.info("Airwatch User: " + user);
		log.info(pwd);

		setHost(host);
		setUser(user);
		setPwd(pwd);

		this.devices = getDevicesFromMDM();
	}

	/**
	 * Liefert eine Liste der Geräte entsprechend der JAXB Objekte
	 * DeviceSearchResult.Devices zurück
	 * 
	 * Da das Airwatch die Liste nicht komplett liefert, sondern nur seitenweise,
	 * sind mehrere Aufrufe notwendig.
	 * 
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws JAXBException
	 * @throws XMLStreamException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 * @throws KeyManagementException
	 */
	public List<DeviceSearchResult.Devices> getDevicesFromMDM()
			throws IOException, ParserConfigurationException, SAXException, JAXBException, XMLStreamException,
			KeyManagementException, KeyStoreException, NoSuchAlgorithmException {

		List<DeviceSearchResult.Devices> devices = new ArrayList<DeviceSearchResult.Devices>();

		while ((MdmPage + 1) * PAGESIZE < MdmTotal) {

			String uri = host + "?page=" + MdmPage + "&pagesize=" + PAGESIZE;
			HashMap<String, String> headers = new HashMap<String, String>();

			// TODO aw-tenant-code als Job Parameter definieren
			headers.put("aw-tenant-code", "P+CGQwPpfF7xLX0OjkFkmnz/I3ViJ5NN43CiLErFBsU=");

			// perform http call
			// InputStream is = httpReq.getHttpResponse(uri, headers, "GET", user, pwd);
			InputStream is = httpReq.getHttpsResponse(uri, headers, "GET", user, pwd);

			// InputStream is = httpReq.getApacheHttpResponse(uri, user, pwd, headers);

			parseMdmPage(devices, is);
			MdmPage += 1;

		}
		logger.info("Total of " + MdmTotal + " devices loaded.");
		return devices;
	}

	/**
	 * Die Methode implementiert einen Stax Parser und benutzt den JAXB unmarschal
	 * Mechanismus, um aus der XML Struktur DeviceSearchResult das entsprechende
	 * Java Objekt zu erstellen.
	 * 
	 * @param devices
	 * @param is
	 * @throws XMLStreamException
	 * @throws JAXBException
	 */
	private void parseMdmPage(List<DeviceSearchResult.Devices> devices, InputStream is)
			throws XMLStreamException, JAXBException {

		boolean isTotal = false;
		boolean isDevice = false;

		// parse xml page with Stax Parser to unmarshal devices XML to Java
		// object

		XMLStreamReader reader = null;
		// read from stream into reader
		reader = XMLInputFactory.newInstance().createXMLStreamReader(is);
		while (reader.hasNext()) {

			switch (reader.getEventType()) {
			case XMLStreamConstants.START_ELEMENT:

				if (reader.getLocalName().equals("Total")) {
					isTotal = true;
				}

				if (reader.getLocalName().equals("DeviceSearchResult")) {

					JAXBContext jaxbContext = JAXBContext.newInstance(DeviceSearchResult.class);
					Unmarshaller jaxbUnMarschaller = jaxbContext.createUnmarshaller();

					isDevice = true;
					DeviceSearchResult dev = (DeviceSearchResult) jaxbUnMarschaller.unmarshal(reader);
					MdmTotal = dev.getTotal();
					for (int i = 0; i < dev.getDevices().size(); i++) {
						devices.add(dev.getDevices().get(i));
						System.out.println(dev.getDevices().get(i).getId());
					}
				}
				break;

			case XMLStreamConstants.END_ELEMENT:
				break;
			case XMLStreamConstants.END_DOCUMENT:
				reader.close();
				break;

			case XMLStreamConstants.CHARACTERS:

				if (isTotal) {
					MdmTotal = Integer.parseInt(reader.getText());
					isTotal = false;
				}

				break;
			}
			if (!isDevice) {
				// Unmarshalling automatically sets the cursor to the next
				// devices element
				// after creating the java object --> skip
				reader.next();
			} else {
				isDevice = false;
			}
		}
	}

}
