//**************************************************
// Copyright (c) 2010,2011,2012,2015 Cisco Systems, Inc.
// All rights reserved.
//**************************************************

package de.peri.sap.po.ejb.citrix.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;



public class JAXBHelper {

	private JAXBContext jaxbContext;
	
	
	
	/**
	 * 
	 * @param objextFactoryName (ObjectFactory.class.getPackage().getName())
	 */
	public JAXBHelper(String objectFactoryPackageName) {
		
		try {
			jaxbContext = JAXBContext.newInstance(objectFactoryPackageName);
		} catch (JAXBException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	
	
	/**
	 * marshal jaxb element class to xml string
	 * @param element
	 * @return
	 * @throws JAXBException
	 */
	public String marshal(JAXBElement<?> element) throws JAXBException {
		
		Marshaller m = jaxbContext.createMarshaller();
		m.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		StringWriter sw = new StringWriter();
		
		m.marshal(element, sw);
		
		return sw.toString();
	}
	

	/**
	 * Unmarshal an input stream into a specific object as defined by the
	 * type (T). Content of the input stream should match the provided type.
	 * @param type - the type of the returned object
	 * @param ins - stream (xml) that holds the object info
	 * @return - a T type object, of the unmarshaled stream
	 * @throws JAXBException
	 * @throws IOException
	 */
	public  <T> T unmarshal(T type, InputStream ins) throws JAXBException, IOException {
		org.xml.sax.InputSource inputSource = new org.xml.sax.InputSource(ins);
		inputSource.setEncoding("UTF-8");
		Unmarshaller um = jaxbContext.createUnmarshaller();
		um.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
		@SuppressWarnings("unchecked")
		JAXBElement<T> result = (JAXBElement<T>)um.unmarshal(inputSource);
		return result.getValue();
	}

	/**
	 * 
	 * @param ins
	 * @return
	 * @throws JAXBException
	 * @throws IOException
	 */
	public Object unmarshal2(InputStream ins) throws JAXBException, IOException {
		
		Unmarshaller um = jaxbContext.createUnmarshaller();
		um.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
		return ((JAXBElement<?>)um.unmarshal(ins)).getValue();
	}
}
